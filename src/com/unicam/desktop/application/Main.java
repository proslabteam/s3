package com.unicam.desktop.application;

import java.io.IOException;
import java.net.URL;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.Pane;


public class Main extends Application {

    private Stage primaryStage;
    private Pane rootLayout;
	@Override
	public void start(Stage primaryStage) {


		 this.primaryStage = primaryStage;
	        this.primaryStage.setTitle("BPMN Checker");
	        this.primaryStage.setOnCloseRequest(e -> System.exit(0));


	        initRootLayout();
	}




	private void initRootLayout() {
		 try {

	            // Load root layout from fxml file.
	            FXMLLoader loader = new FXMLLoader();
//	            loader.setController(new MainController());
	            loader.setLocation(getClass().getResource("Inferface.fxml"));
	            rootLayout = (Pane) loader.load();


	            // Show the scene containing the root layout.
	            Scene scene = new Scene(rootLayout);
	            primaryStage.setScene(scene);
	            primaryStage.show();

	        } catch (IOException e) {
	            e.printStackTrace();
	        }

	}

	public static void main(String[] args) {
		launch(args);
	}
}
