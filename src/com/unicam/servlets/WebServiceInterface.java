package com.unicam.servlets;
import java.io.File;
import java.io.IOException;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.GET;
	import javax.ws.rs.POST;
	import javax.ws.rs.Path;  
	import javax.ws.rs.Produces;  
	import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.commons.io.IOUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


import com.unicam.collaborations.bfs.CollaborationBDF;
@Path("/verifier")
public class WebServiceInterface {
	  


	  // This method is called if HTML and XML is not requested  
	  @GET
	//  @Path("/test") 
	  @Produces(MediaType.TEXT_PLAIN)  
	  public String sayPlainTextHello() {  
	    return "The S3 WebService is correctly running";  
	  }  
	
	  
	  
	  @POST
	  // @Path("/") 
	   @Produces("application/json")
	   public Response getUsers(String model){ 
		 
		
		   CollaborationBDF collaboration = new CollaborationBDF();
			JSONObject verificationResult = null;
			try {
				File f=File.createTempFile(model, null);
				 verificationResult=collaboration.init(IOUtils.toInputStream(model, "UTF-8"), false);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				
			}
			JSONObject models =new JSONObject();
		JSONArray currentModel=new JSONArray();
		currentModel.put(verificationResult);
		
		try {
			models.put("models", currentModel);
				} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
			
	 
			String result =  models.toString();
			return Response.status(200).entity(result).build();
	   }  
	
}
