package com.unicam.collaborations.bfs;



import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Generated;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;

import org.apache.commons.math3.util.Pair;
import org.bouncycastle.asn1.ess.OtherSigningCertificate;
import org.camunda.bpm.model.bpmn.Bpmn;
import org.camunda.bpm.model.bpmn.BpmnModelInstance;
import org.camunda.bpm.model.bpmn.impl.instance.WhileExecutingInputRefs;
import org.camunda.bpm.model.bpmn.instance.EndEvent;
import org.camunda.bpm.model.bpmn.instance.EventBasedGateway;
import org.camunda.bpm.model.bpmn.instance.ExclusiveGateway;
import org.camunda.bpm.model.bpmn.instance.FlowNode;
import org.camunda.bpm.model.bpmn.instance.IntermediateCatchEvent;
import org.camunda.bpm.model.bpmn.instance.MessageFlow;
import org.camunda.bpm.model.bpmn.instance.ParallelGateway;
import org.camunda.bpm.model.bpmn.instance.SequenceFlow;
import org.camunda.bpm.model.bpmn.instance.StartEvent;
import org.camunda.bpm.model.bpmn.instance.SubProcess;
import org.json.JSONException;
import org.json.JSONObject;





public class CollaborationBDF {
	public static void main(String[] args) {
		CollaborationBDF collaboration = new CollaborationBDF();
		//		JFileChooser chooser = new JFileChooser(".");
		//		FileNameExtensionFilter filter = new FileNameExtensionFilter("BPMN", "bpmn");
		//		chooser.setFileFilter(filter);
		//		int returnVal = chooser.showOpenDialog(null);
		//		if (returnVal == JFileChooser.APPROVE_OPTION) {
		//			System.out.println("You chose to open this file: " + chooser.getSelectedFile().getName());
		//		}
//				File file = new File(chooser.getSelectedFile().getAbsolutePath());
		File file=new File("/home/cippus/Documents/eclipse/workspace/S3/From/465541086_rev8.bpmn");
//					File file=new File("/home/cippus/Downloads/13_1552924496673_3013233329212233.bpmn");
		System.out.println(file.getAbsolutePath());

		try {
			collaboration.init(new FileInputStream(file), true);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	Graph<Node> LTS;
    private BpmnModelInstance modelInstance;

	public Graph<Node> getLTS() {
		return LTS;
	}



	public BpmnModelInstance getModelInstance() {
		return modelInstance;
	}



	public CollaborationBDF() {
		LTS=new Graph<Node>();
	}

	

	public JSONObject init(InputStream fileInputStream, boolean b) {
		modelInstance = Bpmn.readModelFromStream(fileInputStream);
		Node root=new Node();
//ELEMENTS COUNT
		int elements = 0;
		for (org.camunda.bpm.model.bpmn.instance.Process p : modelInstance
				.getModelElementsByType(org.camunda.bpm.model.bpmn.instance.Process.class)) {
			elements += p.getChildElementsByType(FlowNode.class).size();
		} 
		
		for (StartEvent startEvent : modelInstance.getModelElementsByType(StartEvent.class)) {
			if (!(startEvent.getParentElement() instanceof SubProcess)) {
				for (SequenceFlow s : startEvent.getOutgoing()) {
					root.addEdge(s);
				}
			}
		}

		long elapsed = System.nanoTime();
		LTS.addVertex(root);
		LTS.unvisited.add(root);
		BFS();

		//System.out.println(LTS.toString());

		//VisualGraph generation
//		VisualGraph graphStream=new VisualGraph("Collaboration");
//		graphStream.showGraph();
//		for (Node n : LTS.map.keySet()) {
//			graphStream.getGraph().addNode(String.valueOf(n.getId())).setAttribute("ui.label", n.toString());
//		}
//		int i=0;
//		for (Node n : LTS.map.keySet()) {
//			for (Node child : LTS.map.get(n).keySet()) {
//				if (child!=null&&n!=null) {
//					graphStream.getGraph().addEdge(String.valueOf(i++),String.valueOf(n.getId()), String.valueOf(child.getId())).setAttribute("ui.label", LTS.map.get(n).get(child));
//				}
//
//			}
//		}

		//verification
		Verifier v=new Verifier(LTS, modelInstance);
		JSONObject verificationResults = null;
		try {
			verificationResults=v.getVerificationResults();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		elapsed =System.nanoTime() - elapsed;
		//System.out.println("\n Lista delle foglie: "+v.getAllLeaf());
//		v.getSequenceIncomingToEndBPMN(modelInstance).stream().forEach(s->System.out.println("All incoming sequence into end "+s.getId()));
//		v.getAllUnsafeSequences().stream().forEach(s->System.out.println("All unsafe model "+s.getId()));;
//		v.endLabelIsNotDuplicated(v.getSequenceIncomingToEndBPMN(modelInstance)).stream().forEach(s->System.out.println("Lista end unsound "+s.getId()));
//		
//		v.getSequenceIncomingToEndGraph().stream().forEach(s->System.out.println("All incoming sequences to the leaf "+s.getId()));;
		
		try {
			verificationResults.put("elements", elements);
			verificationResults.put("time", elapsed);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//System.out.println(LTS.map.size());
		System.out.println(verificationResults.toString());
		return verificationResults;
	}

	private void BFS() {
		Iterator it = LTS.unvisited.iterator();
		int i=0;
		while (it.hasNext()) {
			Node currentNode=(Node) it.next();
//			System.out.print("\n CurrentNode:"+currentNode+"\n");
//			for (SequenceFlow s : currentNode.getReachabilityPath()) {
//				System.out.print("Reachability Path "+s.getId()+", ");
//			}
			for (SequenceFlow sequenceFlow : currentNode.edges.keySet()) {
				FlowNode nextBPMNElement=sequenceFlow.getTarget();
				String sequenceFlowLabel=sequenceFlow.getId();
				Pair<MessageFlow,Boolean> messageFlowInvolved=getMessageFlow(nextBPMNElement);

				//				System.out.print("\n CurrentNode:"+currentNode+" "+sequenceFlow.getSource().getId()+"  "+sequenceFlowLabel+"  "+nextBPMNElement.getId());


				//ENDEVEND
				if (nextBPMNElement instanceof EndEvent) {
					Node nextNode;
					if (nextBPMNElement.getParentElement() instanceof SubProcess) {
						nextNode=generateNode(currentNode,sequenceFlow,null,sequenceFlowLabel);
						boolean isFinisced=true;
						for (SequenceFlow sequencesNextNode : nextNode.edges.keySet()) {
							if (sequencesNextNode.getParentElement() instanceof SubProcess) {
								isFinisced=false;
								break;
							}
						}
						if (isFinisced) {
							SubProcess s=(SubProcess) nextBPMNElement.getParentElement();
							for (SequenceFlow sequenceOutSubprocess:s.getOutgoing()) {
								nextNode.addEdge(sequenceOutSubprocess);
							}
						}

					}
					else {
						nextNode=generateNode(currentNode,sequenceFlow,null,sequenceFlowLabel);
					}
					if(LTS.addEdge(currentNode, nextNode,sequenceFlowLabel)) {
						LTS.unvisited.add(nextNode);
					}

				}

				//SEND MESSAGE ELEMENT
				else if(messageFlowInvolved!=null && messageFlowInvolved.getValue()) {
					for (SequenceFlow outgoingSequence : nextBPMNElement.getOutgoing()) {
						Node nextNode=generateNode(currentNode,sequenceFlow, outgoingSequence ,sequenceFlowLabel);
						nextNode.addMessages(messageFlowInvolved.getKey());

						if(LTS.addEdge(currentNode, nextNode,sequenceFlowLabel)) {
							LTS.unvisited.add(nextNode);
						}
					}
				}	
				//RECEIVE MESSAGE ELEMENT
				else if (messageFlowInvolved!=null && !messageFlowInvolved.getValue()) {
					if (currentNode.messages.containsKey(messageFlowInvolved.getKey())) {
						for (SequenceFlow outgoingSequence : nextBPMNElement.getOutgoing()) {						

							Node nextNode;
							//Remove all other sequence flow outgoing from the eventbase
							if (sequenceFlow.getSource() instanceof EventBasedGateway) {
								nextNode=generateNode(currentNode,null,outgoingSequence,sequenceFlowLabel);
								for (SequenceFlow otherSequence : ((EventBasedGateway) sequenceFlow.getSource()).getOutgoing()) {
									nextNode.removeEdge(otherSequence);
								}
							}else {
								nextNode=generateNode(currentNode,sequenceFlow,outgoingSequence,sequenceFlowLabel);

							}
							nextNode.decreaseMessage(messageFlowInvolved.getKey());
							if(LTS.addEdge(currentNode, nextNode,sequenceFlowLabel)) {
								LTS.unvisited.add(nextNode);

							}

							//manca il caso dell'event base
						}
					}

				}
				
				//INTERMEDIATE CATCH EVENT
				else if(nextBPMNElement instanceof IntermediateCatchEvent) {
					for (SequenceFlow outgoingSequence : nextBPMNElement.getOutgoing()) {						

						Node nextNode;
						//Remove all other sequence flow outgoing from the eventbase
						if (sequenceFlow.getSource() instanceof EventBasedGateway) {
							nextNode=generateNode(currentNode,null,outgoingSequence,sequenceFlowLabel);
							for (SequenceFlow otherSequence : ((EventBasedGateway) sequenceFlow.getSource()).getOutgoing()) {
								nextNode.removeEdge(otherSequence);
							}
						}else {
							nextNode=generateNode(currentNode,sequenceFlow,outgoingSequence,sequenceFlowLabel);

						}
						
						if(LTS.addEdge(currentNode, nextNode,sequenceFlowLabel)) {
							LTS.unvisited.add(nextNode);

						}
					}
				}
				//PARALLEL GATEWAY AND EVENT BASED GATEWAY
				else if(nextBPMNElement instanceof ParallelGateway || nextBPMNElement instanceof EventBasedGateway) {
					//SPLIT
					if (nextBPMNElement.getIncoming().size()==1) {
						Node nextNode=generateNode(currentNode,sequenceFlow, null ,sequenceFlowLabel);
						for (SequenceFlow outgoingSequence : nextBPMNElement.getOutgoing()) {
							nextNode.addEdge(outgoingSequence);
						}

						if(LTS.addEdge(currentNode, nextNode,sequenceFlowLabel)) {
							LTS.unvisited.add(nextNode);
						}
					}

					//CONVERGING
					if (nextBPMNElement.getOutgoing().size()==1) {
						int executed=0;
						for (SequenceFlow incoming  : nextBPMNElement.getIncoming()) {
							if (currentNode.getReachabilityPath().contains(incoming)) {
								executed++;
							}
						}
						if (executed==nextBPMNElement.getIncoming().size()-1) {
							Node nextNode=generateNode(currentNode,sequenceFlow,null,sequenceFlowLabel);
							for (SequenceFlow outgoingEdge : nextBPMNElement.getOutgoing()) {
								nextNode.addEdge(outgoingEdge);
							}
							if(LTS.addEdge(currentNode, nextNode,sequenceFlowLabel)) {
								LTS.unvisited.add(nextNode);
							}
						}
						else {
							Node nextNode=generateNode(currentNode, sequenceFlow, null, sequenceFlowLabel);
							if(LTS.addEdge(currentNode, nextNode,sequenceFlowLabel)) {
								LTS.unvisited.add(nextNode);
							}
						}
					}
				}

				else if(nextBPMNElement instanceof SubProcess){
					for (StartEvent start: nextBPMNElement.getChildElementsByType(StartEvent.class)) {
						for (SequenceFlow outgoing : start.getOutgoing()) {
							Node nextNode=generateNode(currentNode, sequenceFlow, outgoing, sequenceFlowLabel);
							if(LTS.addEdge(currentNode, nextNode,sequenceFlowLabel)) {
								LTS.unvisited.add(nextNode);

							}
						}
					}
				}
				
				else {	
					for (SequenceFlow outgoingSequence : nextBPMNElement.getOutgoing()) {
						Node nextNode=generateNode(currentNode, sequenceFlow, outgoingSequence, sequenceFlowLabel);
						if(LTS.addEdge(currentNode, nextNode,sequenceFlowLabel)) {
							LTS.unvisited.add(nextNode);

						}

					}
				}
			}
			LTS.unvisited.remove(currentNode);
			it = LTS.unvisited.iterator();
			i++;
		}


	}


	//the boolean return indicate if the element isSource TRUE for source
	public Pair<MessageFlow, Boolean> getMessageFlow(FlowNode currentElement){
		for (MessageFlow message : modelInstance.getModelElementsByType(MessageFlow.class)) {
			if (message.getSource().getId().equals(currentElement.getId())) {
				return new Pair<MessageFlow, Boolean>(message, true);
			}else if(message.getTarget().getId().equals(currentElement.getId())){
				return new Pair<MessageFlow, Boolean>(message, false);
			}
		}
		return null;

	}


	public Node generateNode(Node currentNode, SequenceFlow remove, SequenceFlow add, String sequenceFlowLabel) {
		Node nextNode=currentNode.clone();
		nextNode.addReachabilityPath(remove);
		if (remove!=null) {
			nextNode.removeEdge(remove);
		}

		if (add!=null) {
			nextNode.addEdge(add);
		}
		nextNode.setId(LTS.getNodeId());
		return nextNode;
	}
}
