package com.unicam.collaborations.bfs;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.json.JSONException;
import org.json.JSONObject;

public class MassiveTestFromFileList {
	public static void main(String[] args) throws FileNotFoundException {




		FileWriter results = null;
		try {
			results = new FileWriter(new File("resultsWithoutThreadForTimeCalculation.txt"));
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		BufferedReader br = new BufferedReader(new FileReader("ListOfVerifiableFile.txt"));

		Map<Integer, String> codes = new HashMap<Integer,String>(4);
		codes.put(0,"Unsound for dead token");
		codes.put(1,"Unsound for proper completion violation");
		codes.put(2,"Message disregarding sound");
		codes.put(3,"Sound"); 
		codes.put(4,"Safe");
		codes.put(5,"Unsafe"); 
		String file;

		try {
			while((file=br.readLine())!=null){  
				File input = new File("/home/cippus/Documents/eclipse/workspace/S3/From/"+file);
				CollaborationBDF c=new CollaborationBDF();
				JSONObject result=c.init(new FileInputStream(input), false);
				int soundCode = (int) result.getJSONObject("sound").get("id");
				int safeCode = (int) result.getJSONObject("safe").get("id");
				results.write(input.getName() + "\t" + result.get("elements") + "\t" + result.get("time")
				+ "\t" + soundCode + "\t"
				+ result.getJSONObject("sound").get("sequences") + "\t"
				+ result.getJSONObject("sound").get("messages") + "\t"
				+ safeCode + "\t"
				+ result.getJSONObject("safe").get("sequences") + "\t"
				+ result.getJSONObject("safe").get("messages") + "\t"
				+ codes.get(soundCode)+ "\t"
				+ codes.get(safeCode)+ "\n"); 
				results.flush();
			}
		} catch (IOException | JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}  
	}
}













