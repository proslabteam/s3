package com.unicam.collaborations.bfs;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import org.camunda.bpm.model.bpmn.BpmnModelInstance;
import org.camunda.bpm.model.bpmn.instance.EndEvent;
import org.camunda.bpm.model.bpmn.instance.ExclusiveGateway;
import org.camunda.bpm.model.bpmn.instance.FlowNode;
import org.camunda.bpm.model.bpmn.instance.MessageFlow;
import org.camunda.bpm.model.bpmn.instance.SequenceFlow;
import org.camunda.bpm.model.bpmn.instance.SubProcess;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;



public class Verifier {
	Graph<Node> graph;
	BpmnModelInstance modelInstance;
	public Verifier(Graph graph, BpmnModelInstance modelInstance) {
		this.graph=graph;
		this.modelInstance=modelInstance;
	}



	public JSONObject getVerificationResults() throws JSONException {
		JSONObject model=new JSONObject();
		JSONObject safe=new JSONObject();
		JSONArray SeqSound =new JSONArray();
		JSONArray MesSound =new JSONArray();
		JSONArray MesSafe =new JSONArray();
		JSONObject sound=new JSONObject();
		JSONArray SeqSafe =new JSONArray();


		sound.put("sequences", SeqSound);
		sound.put("messages", MesSound);
		safe.put("sequences", SeqSafe);
		safe.put("messages", MesSafe);
		model.put("sound", sound);
		model.put("safe", safe);



		//SOUND
		if (getAllLeaf().size()==1 
				&& endLabelIsNotDuplicated(getSequenceIncomingToEndBPMN(modelInstance)).isEmpty()
				&& getSequenceIncomingToEndBPMN(modelInstance).equals(getSequenceIncomingToEndGraph())
				&& getAllLeaf().stream().allMatch(s -> s.edges.isEmpty())
				&& getAllLeaf().stream().allMatch(s -> s.messages.isEmpty())) {
			sound.put("id", 3);
		}
		//RELAXED SOUND
		else if(endLabelIsNotDuplicated(getSequenceIncomingToEndBPMN(modelInstance)).isEmpty()
				&& getSequenceIncomingToEndBPMN(modelInstance).equals(getSequenceIncomingToEndGraph())
				&& getAllLeaf().stream().allMatch(s -> s.edges.isEmpty())){
			sound.put("id", 2);
			for (Node n : getAllLeaf()) {
				n.messages.keySet().stream().forEach(s ->MesSound.put(s.getId()));
			}
		}
		//UNSOUND PROPER COMPLETION VIOLATED
		else if (getAllLeaf().size()==1 
				&& !endLabelIsNotDuplicated(getSequenceIncomingToEndBPMN(modelInstance)).isEmpty()
				&& getSequenceIncomingToEndBPMN(modelInstance).equals(getSequenceIncomingToEndGraph())
				&& getAllLeaf().stream().allMatch(s -> s.edges.isEmpty())
				&& getAllLeaf().stream().allMatch(s -> s.messages.isEmpty())) {
			sound.put("id", 1);
			endLabelIsNotDuplicated(getSequenceIncomingToEndBPMN(modelInstance)).stream().forEach(s->SeqSound.put(s.getId()));

		}
		else {
			Set<SequenceFlow> unsoundSequences=new HashSet<SequenceFlow>();
			Set<MessageFlow> unsoundMessages=new HashSet<MessageFlow>();
			//dagli incoming delle foglie del grafo tolgo i sequence che portano agli exclusive perchè graficamente non corretto
			unsoundSequences.addAll(getSequenceIncomingToEndGraph().stream().filter(s -> !(s.getTarget() instanceof ExclusiveGateway)).collect(Collectors.toSet()));
			unsoundSequences.removeAll(getSequenceIncomingToEndBPMN(modelInstance));
			unsoundSequences.addAll(getAllUnsafeSequences());			
			for (Node n : getAllLeaf()) {
				unsoundSequences.addAll(n.edges.keySet());
				unsoundMessages.addAll(n.messages.keySet());

			}
			sound.put("id", 0);
			unsoundSequences.stream().forEach(s->SeqSound.put(s.getId()));
			unsoundMessages.stream().forEach(m->MesSound.put(m.getId()));
			System.out.println("");
		}
		//SAFE
		if (getAllUnsafeSequences().isEmpty()) {
			safe.put("id", 4);
		}
		//UNSAFE
		else {
			safe.put("id", 5);
			getAllUnsafeSequences().stream().forEach(s->SeqSafe.put(s.getId()));
		}
		
		return model;

	}




	//Tutte le foglie del grafo
	public ArrayList<Node> getAllLeaf(){
		ArrayList<Node> leafs=new ArrayList<Node>();
		for (Node n : graph.map.keySet()) {
			if(graph.map.get(n).isEmpty()) {
				leafs.add(n);
			}
		}
		return leafs;
	}
	//ritorna tutti i sequenceflow con più di due token
	public Set<SequenceFlow> getAllUnsafeSequences(){
		Set<SequenceFlow> unsafeSequences=new HashSet<SequenceFlow>();
		for (Node n : graph.map.keySet()) {
			for (SequenceFlow seq: n.edges.keySet()) {
				if(n.edges.get(seq)>1) {
					unsafeSequences.add(seq);
				}
			}
		}
		return unsafeSequences;
	}

	//ritorna tutti i sequence flow incoming in tutti gli end event
	public Set<SequenceFlow> getSequenceIncomingToEndBPMN(BpmnModelInstance modelInstance){
		Set<SequenceFlow> incomingOfEndEvents=new HashSet<SequenceFlow>();

		for (FlowNode endEventBPMN : modelInstance.getModelElementsByType(EndEvent.class)) {
			if (!(endEventBPMN.getParentElement() instanceof SubProcess)) {
				incomingOfEndEvents.addAll(endEventBPMN.getIncoming());
			}
		}
		return  incomingOfEndEvents;
	}
	//Controlla se gli gli incoming sequence flow sono ripetuti nel grafo
	public Set<SequenceFlow> endLabelIsNotDuplicated(Set<SequenceFlow> set) {
		Set<SequenceFlow> duplicateSequenceFlow=new HashSet<SequenceFlow>();
		for (Node leaf:getAllLeaf()) {
			List<SequenceFlow> reachability=leaf.getReachabilityPath();
			for (SequenceFlow sequenceFlow : set) {
				if(findDuplicates(reachability).contains(sequenceFlow)) {
					duplicateSequenceFlow.add(sequenceFlow);
				}
			}
		}
		return duplicateSequenceFlow;
	}

	//rinorna i sequence flow che portano alle foglie
	public Set<SequenceFlow> getSequenceIncomingToEndGraph() {
		ArrayList<Node>endNode=getAllLeaf();
		Set<SequenceFlow> labelToEndEvent=new HashSet<SequenceFlow>();
		for (Node n : endNode) {
			for (Entry<Node, HashMap<Node, String>> en: graph.map.entrySet()) {
				for (Node node : en.getValue().keySet()) {
					if (node.equals(n)) {
						labelToEndEvent.add(modelInstance.getModelElementById(en.getValue().get(node)));
					}

				}
			}
		}
		return labelToEndEvent;

	}

	//
	private <T> Set<T> findDuplicates(Collection<T> collection) {
		Set<T> uniques = new HashSet<>();
		return collection.stream()
				.filter(e -> !uniques.add(e))
				.collect(Collectors.toSet());
	}



}
